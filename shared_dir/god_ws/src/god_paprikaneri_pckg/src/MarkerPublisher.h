#include <ros/ros.h>
#include <std_msgs/String.h>
#include <visualization_msgs/Marker.h>
#include "GridCell.h"

extern int width;
extern int length;
extern float cellSize;

extern int w;
extern int l;


extern GridCell **retVal;

class MarkerPublisher
{
  public:
    MarkerPublisher(ros::NodeHandle& n);

    void SetMarkerOptions(int i, int j, int pointCount, float maxHeight);
    void SetMarkerPose(int i, int j, float maxHeight);
    void SetMarkerScale(float maxHeight);
    void SetMarkerColor(int pointCount);

    void PublishMarker();

  private:
    ros::NodeHandle& node_handle_;
    ros::Publisher marker_pub_;
    visualization_msgs::Marker marker_;
};
