#include <stdio.h>
#include <stdlib.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include "GridCell.h"
#include "MarkerPublisher.h"
#include "PointCloudListener.h"

int width = 12;
int length = 8;
float cellSize = 1.0f;

int w = width / 2;
int l = length / 2;

GridCell **retVal;

void setTransformMatrix(tf::StampedTransform& transform_matrix);

int main(int argc, char **argv)
{

    ros::init(argc, argv, "listener");

    ros::NodeHandle n;
    ros::Rate r(22);

    tf::StampedTransform transform_matrix;

    retVal =  new GridCell* [length];
    for(int i = 0; i < length; i++)
    {
        retVal[i] = new GridCell[width];
    }

    setTransformMatrix(transform_matrix);

    PointCloudListener point_cloud_listener_(n, transform_matrix);
    MarkerPublisher marker_publisher_(n);

    point_cloud_listener_.Init();

    while(ros::ok())
    {
        for(int i = 0; i < length; i++){
            for(int j = 0; j < width; j++){
                marker_publisher_.SetMarkerOptions(i, j, retVal[i][j].pointCounter, retVal[i][j].maxHeight);
                marker_publisher_.PublishMarker();
                retVal[i][j].pointCounter = 0;
                retVal[i][j].maxHeight = 0.0;
            }
        }

        ros::spinOnce();
        r.sleep();
    }
    
    return 0;
}

void setTransformMatrix(tf::StampedTransform& transform_matrix)
{
    tf::TransformListener transform_listener;

    try
    {
        transform_listener.waitForTransform("/base_link", "/velodyne", ros::Time(0), ros::Duration(10.0) );
        transform_listener.lookupTransform("/base_link", "/velodyne", ros::Time(0), transform_matrix);
    }
    catch (tf::TransformException ex)
    {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
    }
}