#include "GridCell.h"
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud2.h"
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/conditional_removal.h>

extern int width;
extern int length;
extern float cellSize;

extern int w;
extern int l;

class PointCloudListener
{
  public:
    PointCloudListener(ros::NodeHandle& n, tf::StampedTransform& transform_matrix);

    void ReceivePointCloudMessage() const;
    void Init();
    void PointCloudListenerCallback(const boost::shared_ptr<const sensor_msgs::PointCloud2>& input);

  private:
    ros::NodeHandle& node_handle_;
    tf::StampedTransform& transform_matrix_;
    ros::Subscriber sub_;
    pcl::ConditionAnd<pcl::PointXYZ>::Ptr range_cond_;
};
