#include "PointCloudListener.h"
#include <cmath>

extern GridCell **retVal;

void Processing (const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered)
{
    for (int i = 0; i < cloud_filtered->points.size(); ++i)
    {
        if((cloud_filtered->points[i].x > 1.0 || cloud_filtered->points[i].x < -1.0) &&
         (cloud_filtered->points[i].y > 2.0 || cloud_filtered->points[i].y < -2.0)){
            int xCoordinate = floor(cloud_filtered->points[i].x + w*cellSize);
            int yCoordinate = floor(-cloud_filtered->points[i].y + l*cellSize);

            retVal[yCoordinate][xCoordinate].pointCounter++;        
            
            if (retVal[yCoordinate][xCoordinate].maxHeight < cloud_filtered->points[i].z)
            {
                retVal[yCoordinate][xCoordinate].maxHeight = cloud_filtered->points[i].z;
            }
        }
    }
}

PointCloudListener::PointCloudListener(ros::NodeHandle& n, tf::StampedTransform& transform_matrix) : node_handle_(n), transform_matrix_(transform_matrix), sub_(node_handle_.subscribe("points_raw", 1000, &PointCloudListener::PointCloudListenerCallback, this)), range_cond_(new pcl::ConditionAnd<pcl::PointXYZ> ())
{
}

void PointCloudListener::PointCloudListenerCallback(const boost::shared_ptr<const sensor_msgs::PointCloud2>& input)
{
    pcl::PCLPointCloud2 pcl_pc2;
    pcl_conversions::toPCL(*input,pcl_pc2);
    pcl::PointCloud<pcl::PointXYZ>::Ptr temp_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromPCLPointCloud2(pcl_pc2,*temp_cloud);
    pcl_ros::transformPointCloud(*temp_cloud, *transformed_cloud, transform_matrix_);

     // build the filter
    pcl::ConditionalRemoval<pcl::PointXYZ> condrem;
    condrem.setCondition (range_cond_);
    condrem.setInputCloud (transformed_cloud);

    condrem.filter(*cloud_filtered);

    Processing(cloud_filtered);
}

void PointCloudListener::Init()
{
    range_cond_->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::GT, -w)));
    range_cond_->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::LT, w)));

    range_cond_->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::GT, -l)));
    range_cond_->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::LT, l)));

    range_cond_->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::GT, 0.3)));
    range_cond_->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::LT, 6.0)));
}
