#include "MarkerPublisher.h"

MarkerPublisher::MarkerPublisher(ros::NodeHandle& n) : node_handle_(n), marker_(), marker_pub_(node_handle_.advertise<visualization_msgs::Marker>("visualization_marker", 1))
{
}

void MarkerPublisher::SetMarkerOptions(int i, int j, int pointCount, float maxHeight)
{
    marker_.header.frame_id = "/base_link";
    marker_.header.stamp = ros::Time::now();

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    marker_.ns = "basic_shapes";
    if((marker_.id++) == width*length)
    {
        marker_.id = 0;
    };

    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    marker_.type = visualization_msgs::Marker::CUBE;;

    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    marker_.action = visualization_msgs::Marker::ADD;

    SetMarkerPose(i, j, maxHeight);
    SetMarkerScale(maxHeight);
    SetMarkerColor(pointCount);

    marker_.lifetime = ros::Duration();
}

void MarkerPublisher::SetMarkerPose(int i, int j, float maxHeight)
{
    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker_.pose.position.x = j - w*cellSize + 1.5* cellSize;
    marker_.pose.position.y = -1 * (i - l*cellSize) - 0.5*cellSize;
    marker_.pose.position.z = maxHeight / 2;
    marker_.pose.orientation.x = 0.0;
    marker_.pose.orientation.y = 0.0;
    marker_.pose.orientation.z = 0.0;
    marker_.pose.orientation.w = 1.0;
}

void MarkerPublisher::SetMarkerScale(float maxHeight)
{
    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker_.scale.x = cellSize;
    marker_.scale.y = cellSize;
    marker_.scale.z = maxHeight;
}

void MarkerPublisher::SetMarkerColor(int pointCount)
{
    // Set the color -- be sure to set alpha to something non-zero!
    marker_.color.r = 0.0f;
    marker_.color.g = 1.0f;
    marker_.color.b = 0.0f;
    marker_.color.a = pointCount/300.0;
}

void MarkerPublisher::PublishMarker()
{
    marker_pub_.publish(marker_);
}
